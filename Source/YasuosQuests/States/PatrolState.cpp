// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "PatrolState.h"
#include "WaitState.h"
#include "ChaseState.h"
#include "InspectState.h"


// Sets default values
APatrolState::APatrolState()
{
	patrolLocation.Location = FVector::ZeroVector;
	patrolRadius = 3000.f;
}


void APatrolState::enter(ABaseEnemy* enemy){
	GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Cyan, "Patrol state");

	UNavigationSystem* NavSys = UNavigationSystem::GetCurrent(enemy->GetWorld());
	if (NavSys) {

		enemy->Walk();
		NavSys->GetRandomReachablePointInRadius(enemy->GetActorLocation(), patrolRadius, patrolLocation);
		NavSys->SimpleMoveToLocation(enemy->Controller, patrolLocation.Location);
	}
}

IIEnemyState* APatrolState::Update(ABaseEnemy* enemy) {

	APawn* target = enemy->GetTarget();
	if (target) {
		return NewObject<ABaseEnemyState>(this, AInspectState::StaticClass());
	}

	 if (FVector::Dist(enemy->GetActorLocation(), patrolLocation.Location) <= 600.f) {

		 return NewObject<ABaseEnemyState>(this, AInspectState::StaticClass());
	 }

	return nullptr;
}
