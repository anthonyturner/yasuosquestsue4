// Copyright 2017 @Anthony Turner

#pragma once

#include "States/BaseEnemyState.h"
#include "InspectState.generated.h"

/**
 * 
 */
UCLASS()
class YASUOSQUESTS_API AInspectState : public ABaseEnemyState
{
	GENERATED_BODY()
	

public:
	// Sets default values for this actor's properties
	AInspectState();

	virtual IIEnemyState* Update(ABaseEnemy* enemy)  override;
	virtual void enter(ABaseEnemy* enemy) override;

private:

	FAIMoveRequest faiMR;
	UPROPERTY()
		FTimerHandle seekTimerHandle;

	UPROPERTY()
		UNavigationSystem* NavSys;

	UPROPERTY()
		AAIController* aiController;

	UPROPERTY()
		UWorld* world;

	float inspectTime = 8.f;
	float currentInspectTime;
};
