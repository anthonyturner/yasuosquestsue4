// Copyright 2017 @Anthony Turner

#pragma once
#include "GameFramework/Actor.h"
#include "Interfaces/IEnemyState.h"
#include "BaseEnemyState.generated.h"

/**
 * 
 */
UCLASS()
class YASUOSQUESTS_API ABaseEnemyState : public AActor, public IIEnemyState
{
	GENERATED_BODY()


public:
	// Sets default values for this actor's properties
	ABaseEnemyState();

	virtual IIEnemyState* Update(ABaseEnemy* enemy)  override;
	virtual void enter(ABaseEnemy* enemy) override;

};
