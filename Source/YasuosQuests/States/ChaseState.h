// Copyright 2017 @Anthony Turner

#pragma once

#include "States/BaseEnemyState.h"
#include "ChaseState.generated.h"

/**
 * 
 */
UCLASS()
class YASUOSQUESTS_API AChaseState : public ABaseEnemyState
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AChaseState();

	virtual IIEnemyState* Update(ABaseEnemy* enemy)  override;
	virtual void enter(ABaseEnemy* enemy) override;

private:

	FAIMoveRequest faiMR;
	UPROPERTY()
		FTimerHandle seekTimerHandle;

	UPROPERTY()
	UNavigationSystem* NavSys;

	UPROPERTY()
		AAIController* aiController;
};
