// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "WaitState.h"
#include "PatrolState.h"
#include "ChaseState.h"
#include "InspectState.h"
#include "AttackState.h"

AWaitState::AWaitState()
{
	waitTime = 0.f;

}


void AWaitState::enter(ABaseEnemy* enemy)
{

	GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Yellow, "wait state");
	world = enemy->GetWorld();
	randWaitTime = FMath::RandRange(MIN_WAIT_TIME, MAX_WAIT_TIME);//Get random wait time for this states life duration
}


IIEnemyState* AWaitState::Update(ABaseEnemy* enemy) {

	float deltaSeconds = world->DeltaTimeSeconds;
	waitTime += deltaSeconds;

	if (waitTime >= randWaitTime) {
		return NewObject<ABaseEnemyState>(this, AInspectState::StaticClass());
	}

	APawn* target = enemy->GetTarget();
	if (target && waitTime > enemy->enemyInfo.AttackWaitTime) {

		return NewObject<ABaseEnemyState>(this, AAttackState::StaticClass());
	}
	
	return nullptr;
}

