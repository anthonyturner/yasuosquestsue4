// Fill out your copyright notice in the Description page of Project Settings.

#include "YasuosQuests.h"
#include "BaseStats.h"


UBaseStats::UBaseStats() {

	Speed = 20;
	HitPoints = 1;
	experience = 2;
	BaseAttackDamage = 1;
	AttackTimeout = 4.f;
	TimeSinceLastStrike = 0;
	patrolWaitTime = 3.f;
	chaseRange = 1000.f;
	BaseDefense = 10;
}

float UBaseStats::GetHitPoints(){
	return HitPoints;
}

void UBaseStats::SetHitPoints(float points){

	HitPoints = points;
}


int32 UBaseStats::GetExperience()
{
	return experience;
}

void UBaseStats::SetExperience(int32 exp)
{

	experience = exp;
}
