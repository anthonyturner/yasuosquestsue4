// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "LootComponent.h"
#include "GameModes/YQGameInstance.h"
#include "Data/FWeaponInfo.h"
#include "Characters/Enemies/BaseEnemy.h"

// Sets default values for this component's properties
ULootComponent::ULootComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
}


// Called when the game starts
void ULootComponent::BeginPlay()
{
	Super::BeginPlay();
	PrimaryComponentTick.bCanEverTick = false;
	GetLootItems();
}

void ULootComponent::GetLootItems() {
	

	ABaseEnemy* owner = Cast<ABaseEnemy>(GetOwner());
	UYQGameInstance* gameInstance = Cast<UYQGameInstance>(owner->GetGameInstance());
	int weaponTableSize = gameInstance->GetWeaponTableSize() + 1;
	//maxTryLootItems determines the max chances to add weapons to the enemies loot table
	for (int i = 1; i < maxTryLootItems; i++) {

		FString id;
		id.AppendInt(FMath::RandRange(1, weaponTableSize));

		FString IndexString = FString::FromInt((int32)i);
		FName IndexName = FName(*IndexString);

		FWeaponInfoStruct* weaponInfo = gameInstance->FetchWeapon(IndexName);
		if (weaponInfo) {
			if (weaponInfo->level <= owner->enemyInfo.level) { //Only can use items with a level the same or less than the owner/enemy

				float rand_add_chance = FMath::RandRange(0.f, 1.f);
				if (rand_add_chance > addChance) { //Probablity the item will be added to the enemies loot table
					FItemInfoStruct itemInfo;
					itemInfo.CreateItem(*weaponInfo);
					lootTable.Add(itemInfo);
				} 
			}
		}
	}
}

void ULootComponent::CalculateLoot() {

	float rand_drop_chance = FMath::RandRange(0.f, 1.f);

	if (rand_drop_chance > dropChance) {
		return;
	}
	
	float itemWeights = 0.f;

	for (int i = 0; i < lootTable.Num(); i++) {

		itemWeights += lootTable[i].weight;
	}

	float rand_weight = FMath::RandRange(0.f,itemWeights);
	for (int i = 0; i < lootTable.Num(); i++) {

		FItemInfoStruct itemInfo = lootTable[i];
		if (rand_weight <= itemInfo.weight) {

			DropLoot(itemInfo);
		}
	}
 }


void  ULootComponent::DropLoot(FItemInfoStruct loot) {

	AActor* owner = GetOwner();
	if (owner) {

		ABaseItem* item = GetWorld()->SpawnActor<ABaseItem>(loot.itemClass, owner->GetActorLocation(), owner->GetActorRotation());
		item->interactableMesh->SetSkeletalMesh(loot.itemSKMesh);
		item->interactableMesh->SetSimulatePhysics(true);
		item->interactableMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		item->itemInfo = loot;
		
		UYQGameInstance* gameInstance = Cast<UYQGameInstance>(owner->GetGameInstance());

		FName weaponName;
		FWeaponInfoStruct* weaponInfo = nullptr;

		switch (item->itemInfo.itemType) {//Since the extended item info is not stored, fetch it from the database

			case EItemType::Armor: //Equip Armor

				break;
			case EItemType::Weapon://Equip/attach weapon

				
				weaponInfo = gameInstance->FetchWeapon(item->itemInfo.item_id);
				ABaseWeapon* weapon = Cast<ABaseWeapon>(item);
				weapon->weaponInfo = *weaponInfo;
				weapon->interactableMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
				weapon->interactableMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Destructible, ECollisionResponse::ECR_Ignore);

				break;
		}
	}
 }
 

