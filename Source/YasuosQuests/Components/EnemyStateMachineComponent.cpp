// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "States/PatrolState.h"
#include "EnemyStateMachineComponent.h"

// Sets default values for this component's properties
UEnemyStateMachineComponent::UEnemyStateMachineComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UEnemyStateMachineComponent::BeginPlay()
{
	Super::BeginPlay();
	AActor* owner = GetOwner();
	ABaseEnemy* ai = Cast<ABaseEnemy>(owner);
	if (ai) {

		aiEnemy = ai;
		IIEnemyState* newState = NewObject<ABaseEnemyState>(this, APatrolState::StaticClass());
		ChangeState(newState);

	}
}

// Called every frame
void UEnemyStateMachineComponent::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	if (aiEnemy->IsAlive()) {

		IIEnemyState* newState = currentState->Update(aiEnemy);
		if (newState != nullptr) {

			ChangeState(newState);
		}
	}
}


void UEnemyStateMachineComponent::ChangeState(IIEnemyState * pNewState)
{

	currentState = Cast<ABaseEnemyState>(pNewState);
	currentState->enter(aiEnemy);
}



