// Copyright 2017 @Anthony Turner

#pragma once

#include "Components/ActorComponent.h"
#include "Interfaces/ILootable.h"
#include "Interactables/BaseItem.h"
#include "Interactables/Weapons/BaseWeapon.h"
#include "Interactables/Armor/BaseArmor.h"

#include "LootComponent.generated.h"

class ABaseEnemy;
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class YASUOSQUESTS_API ULootComponent : public UActorComponent, public IILootable
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	ULootComponent();

	// Called when the game starts
	virtual void BeginPlay() override;
	virtual void CalculateLoot() override;
	virtual void DropLoot(FItemInfoStruct loot) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Loot)
		TArray<FItemInfoStruct>lootTable;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Loot)
		float dropChance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Loot)
	int maxTryLootItems = 4;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Loot)
		float addChance;//Chance for enemies to have items/more items

private:

	void GetLootItems();
	
};
