// Copyright 2017 @Anthony Turner

#pragma once

#include "Components/ActorComponent.h"
#include "States/BaseEnemyState.h"

#include "EnemyStateMachineComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class YASUOSQUESTS_API UEnemyStateMachineComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UEnemyStateMachineComponent();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
	ABaseEnemyState* currentState;
	
	void ChangeState(IIEnemyState * pNewState);

	UPROPERTY()
	ABaseEnemy* aiEnemy;

	//UPROPERTY()
	//IIEnemyState* aiEnemy;

};
