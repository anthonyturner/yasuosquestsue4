// Fill out your copyright notice in the Description page of Project Settings.

#include "YasuosQuests.h"
#include "YQGameInstance.h"
#include "Data/FItemInfo.h"
#include "Data/FSpellsData.h"

#include "Data/FWeaponInfo.h"

#include "Interactables/Weapons/BaseWeapon.h"

UYQGameInstance::UYQGameInstance() {


}

void UYQGameInstance::Init() {

	if (this->isInitialized) return;
	this->isInitialized = true;

	 dataTables = NewObject<AGameDataTables>(AGameDataTables::StaticClass());
	
					

}



FWeaponInfoStruct* UYQGameInstance::FetchWeapon(FName name) {

	return dataTables->GetWeapon(name);

}

FSpellsData* UYQGameInstance::FetchSpell(FName name) {

	return dataTables->GetSpell(name);

}


int UYQGameInstance::GetWeaponTableSize() {

	return dataTables->GetWeaponTableSize();

}