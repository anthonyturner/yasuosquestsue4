// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameModeBase.h"
#include "Characters/Enemies/BaseEnemy.h"
#include "GameModes/YQGameInstance.h"
#include "YQGameMode.generated.h"


/**
 * 
 */
UCLASS()
class YASUOSQUESTS_API AYQGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
	
public:
	AYQGameMode();
	virtual void BeginPlay() override;

	UYQGameInstance* instance;

	 int GetEnemyID();
	 int enemyID;

};
