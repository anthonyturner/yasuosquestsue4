// Copyright 2017 @Anthony Turner

#pragma once
#include "Interactables/BaseItem.h"

#include "IRecorderComm.generated.h"

class ACharacterRecorder;
UINTERFACE(MinimalAPI)
class UIRecorderComm : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

/**
 * 
 */
class YASUOSQUESTS_API IIRecorderComm
{
	GENERATED_IINTERFACE_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	virtual void PassReference(ACharacterRecorder* reference) = 0;
	virtual void EquipItem(FItemInfoStruct item) = 0;
	virtual void UnEquipItem(FItemInfoStruct item) = 0;

};
