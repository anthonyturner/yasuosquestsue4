// Copyright 2017 @Anthony Turner

#pragma once

#include "IAttack.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UIAttack : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

/**
 * 
 */
class YASUOSQUESTS_API IIAttack
{
	GENERATED_IINTERFACE_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	virtual void Attack() = 0;
	virtual void ResetAttack() = 0;
};
