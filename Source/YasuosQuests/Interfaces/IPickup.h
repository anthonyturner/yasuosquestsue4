// Copyright 2017 @Anthony Turner

#pragma once

#include "IPickup.generated.h"

class AYQCharacter;
UINTERFACE(MinimalAPI)
class UIPickup : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

/**
 * 
 */
class YASUOSQUESTS_API IIPickup
{
	GENERATED_IINTERFACE_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	virtual void Pickup(AYQCharacter* player) = 0;

};
