// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "IEnemyState.h"


// This function does not need to be modified.
UIEnemyState::UIEnemyState(const class FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
}

// Add default functionality here for any IIEnemyState functions that are not pure virtual.
