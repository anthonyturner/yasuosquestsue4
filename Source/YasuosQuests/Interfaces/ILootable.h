// Copyright 2017 @Anthony Turner

#pragma once
#include "Data/FItemInfo.h"
#include "ILootable.generated.h"

UINTERFACE(MinimalAPI)
class UILootable : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class YASUOSQUESTS_API IILootable
{
	GENERATED_IINTERFACE_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	virtual void CalculateLoot() = 0;
	virtual void DropLoot(FItemInfoStruct loot) = 0;
	
};
