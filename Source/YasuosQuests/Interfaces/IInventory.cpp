// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "IInventory.h"


// This function does not need to be modified.
UIInventory::UIInventory(const class FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
}

// Add default functionality here for any IIInventory functions that are not pure virtual.
