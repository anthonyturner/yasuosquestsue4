// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "IRecorderComm.h"


// This function does not need to be modified.
UIRecorderComm::UIRecorderComm(const class FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
}

// Add default functionality here for any IIRecorderComm functions that are not pure virtual.
