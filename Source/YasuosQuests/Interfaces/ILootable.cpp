// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "ILootable.h"


// This function does not need to be modified.
UILootable::UILootable(const class FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
}

// Add default functionality here for any IILootable functions that are not pure virtual.
