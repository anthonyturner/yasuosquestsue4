// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UObject/NoExportTypes.h"
#include "BaseStats.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class YASUOSQUESTS_API UBaseStats : public UObject{
	GENERATED_BODY()
	
public:

	UBaseStats();
	// How fast he is
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BaseStatsProperties)
	float Speed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BaseStatsProperties)
	float chaseRange;

	// The hitpoints the monster has
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BaseStatsProperties)
		float HitPoints;
		float GetHitPoints();
		void SetHitPoints(float);

		
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BaseStatsProperties)
		float patrolWaitTime;

	// Experience gained for defeating
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BaseStatsProperties)
	int32 experience;
	int32 GetExperience();
	void SetExperience(int32 exp);

	// The amount of damage attacks do
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BaseStatsProperties)
		float BaseAttackDamage;

	// The amount protection against damage attacks
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BaseStatsProperties)
		float BaseDefense;


	// Amount of time the monster needs to rest in seconds
	// between attacking
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BaseStatsProperties)
	float AttackTimeout;

	// Time since monster's last strike, readable in blueprints
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = BaseStatsProperties)
		float TimeSinceLastStrike;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = BaseStatsProperties)
		float WALK_SPEED = 95.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = BaseStatsProperties)
		float RUN_SPEED = 375.f;

	
};
