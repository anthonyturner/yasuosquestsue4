// Copyright 2017 @Anthony Turner

#pragma once

#include "GameFramework/Actor.h"
#include "Interfaces/IInteractable.h"
#include "BaseInteractables.generated.h"

UCLASS()
class YASUOSQUESTS_API ABaseInteractables : public AActor, public IIInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseInteractables();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
	virtual void PostInitializeComponents() override;


	virtual void RegisterInteraction(IIInteractable* interact) override;
	virtual void UnRegisterInteraction(IIInteractable* interact) override;
	virtual void Interact(APawn* instig) override;
	virtual void SetHighlight(bool) override;

	//Image for the interactable item
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Interactable)
	USkeletalMeshComponent* interactableMesh;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Interactable)
	USphereComponent* interactableArea;
	
	UPROPERTY()
		USceneComponent* rootInteractableComponent;

	UFUNCTION()
	void OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION()
		void OnComponentEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

};
