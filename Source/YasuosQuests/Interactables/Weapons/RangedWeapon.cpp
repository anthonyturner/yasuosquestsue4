// Copyright 2017 @Anthony Turner

#include "YasuosQuests.h"
#include "RangedWeapon.h"

void ARangedWeapon::Attack() {

	AActor* weaponOwner = GetOwner();
	if (weaponOwner) {
		// try and fire a projectile
		if (ProjectileClass != NULL){
			UWorld* const World = weaponOwner->GetWorld();
			if (World){

				FActorSpawnParameters SpawnParams;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = Instigator;

			
				FRotator SpawnRotation;
				FVector SpawnLocation;
				SpawnLocation = interactableMesh->GetSocketLocation(WeaponSocket);
				SpawnRotation = weaponOwner->GetActorRotation();
				FTransform transform(SpawnRotation, (SpawnRotation.Vector().ForwardVector *80.f) + SpawnLocation);

				ABaseProjectile* Projectile = World->SpawnActor<ABaseProjectile>(ProjectileClass, transform, SpawnParams);
				if (Projectile){
					
					if (Projectile->projectileInfo.LaunchVFX)
						UGameplayStatics::SpawnEmitterAtLocation(World, Projectile->projectileInfo.LaunchVFX, SpawnLocation);

					if (Projectile->projectileInfo.LaunchSFX)
						UGameplayStatics::PlaySoundAtLocation(World, Projectile->projectileInfo.LaunchSFX, SpawnLocation);

					// find launch direction
					
					FVector const LaunchDir = SpawnRotation.Vector();
					Projectile->InitVelocity(LaunchDir);

				}
			}
		}
	}
	
}