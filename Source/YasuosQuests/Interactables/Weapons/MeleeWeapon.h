// Copyright 2017 @Anthony Turner

#pragma once

#include "Interactables/Weapons/BaseWeapon.h"
#include "Characters/Enemies/BaseEnemy.h"
#include "MeleeWeapon.generated.h"

/**
 * 
 */
UCLASS()
class YASUOSQUESTS_API AMeleeWeapon : public ABaseWeapon
{
	GENERATED_BODY()


public:

	AMeleeWeapon();
		virtual void Attack() override;
		virtual void ResetAttack() override;
private:

	UPROPERTY()
	AActor* damagedActor;
	
};
