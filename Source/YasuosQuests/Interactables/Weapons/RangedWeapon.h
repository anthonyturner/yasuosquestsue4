// Copyright 2017 @Anthony Turner

#pragma once

#include "Interactables/Weapons/BaseWeapon.h"
#include "Interactables/BaseProjectile.h"

#include "RangedWeapon.generated.h"

/**
 * 
 */
UCLASS()
class YASUOSQUESTS_API ARangedWeapon : public ABaseWeapon
{
	GENERATED_BODY()
	
	
public:
		virtual void Attack() override;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RangedWeaponAttributes, meta = (ToolTip = ""))
			TSubclassOf<ABaseProjectile>ProjectileClass;


		UPROPERTY()
		FVector WeaponOffset;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RangedWeaponAttributes, meta = (ToolTip = ""))
		FName WeaponSocket;

};
