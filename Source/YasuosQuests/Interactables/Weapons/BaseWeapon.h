// Copyright 2017 @Anthony Turner

#pragma once

#include "Interactables/Pickups/PickupItem.h"
#include "Interfaces/IAttack.h"

#include "Data/FWeaponInfo.h"
#include "BaseWeapon.generated.h"

class AYQCharacter;
UCLASS()
class YASUOSQUESTS_API ABaseWeapon : public APickupItem, public IIAttack
{
	GENERATED_BODY()


public:
	ABaseWeapon();

	virtual void BeginPlay() override;
	virtual void Attack() override;
	virtual void ResetAttack() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = WeaponAttributes)
		FWeaponInfoStruct weaponInfo;

};