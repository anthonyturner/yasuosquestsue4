#include "YasuosQuests.h"
#include "FItemInfo.h"

void FItemInfoStruct::CreateItem(FWeaponInfoStruct weaponInfo) {


	item_id = weaponInfo.weapon_id;
	itemName = weaponInfo.weapon_name;
	itemValue = weaponInfo.weaponCost;
	itemImage = weaponInfo.weaponImage;
	itemSKMesh = weaponInfo.weaponMesh;
	itemClass = weaponInfo.weaponClass;
	weight = weaponInfo.weapon_weight;
	itemType = EItemType::Weapon;
	itemSlot = ESlotType::PrimaryRightHand;
	itemAnimation = weaponInfo.weaponAnimation;
}


//void FItemInfoStruct::CreateItem(FArmorInfoStruct weaponInfo) {}