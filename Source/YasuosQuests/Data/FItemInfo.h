#pragma once
#include "FWeaponInfo.h"
//#include "FArmorInfo.h"

#include "FItemInfo.generated.h"

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EItemType : uint8
{
	None 	UMETA(DisplayName = "None"),
	Weapon 	UMETA(DisplayName = "Weapon"),
	Projectile 	UMETA(DisplayName = "Projectile"),
	Armor 	UMETA(DisplayName = "Armor"),
	Food	UMETA(DisplayName = "Food"),
	Potion	UMETA(DisplayName = "Potion"),
	Coins	UMETA(DisplayName = "Coins")


};

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class ESlotType : uint8
{
	None 	UMETA(DisplayName = "None"),
	Head 	UMETA(DisplayName = "Head"),
	Torso	UMETA(DisplayName = "Torso"),
	Arms	UMETA(DisplayName = "Arms"),
	Legs	UMETA(DisplayName = "Legs"),
	Feet	UMETA(DisplayName = "Feet"),
	PrimaryRightHand	UMETA(DisplayName = "PrimaryRightHand"),
	PrimaryLeftHand	UMETA(DisplayName = "PrimaryLeftHand"),
	SecondaryRightHand	UMETA(DisplayName = "SecondaryRightHand"),
	SecondaryLeftHand	UMETA(DisplayName = "SecondaryLeftHand")


};
USTRUCT(BlueprintType)
struct FItemInfoStruct : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemAttributes)
		FName item_id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemAttributes)
		USkeletalMesh* itemSKMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemAttributes)
		EItemType itemType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemAttributes)
		FName itemName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemAttributes)
		UTexture2D* itemImage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemAttributes)
		int itemIndex;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemAttributes)
		UClass* itemClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemAttributes)
		ESlotType itemSlot;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemAttributes)
		int itemValue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemAttributes)
		float weight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemAttributes)
		USoundBase* itemPickupSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemAttributes)
		USoundBase*	itemEquipSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemAttributes)
		USoundBase*	itemUnEquipSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemAttributes)
		USoundBase*	itemDropSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemAttributes)
		UAnimationAsset* itemAnimation;

	//default constructor
	FItemInfoStruct()
	{
		itemPickupSound = nullptr;
		itemClass = nullptr;
		itemPickupSound = nullptr;
		itemEquipSound = nullptr;
		itemUnEquipSound = nullptr;
		itemDropSound = nullptr;
	}

	//For GC
	void Destroy()
	{
		itemPickupSound = nullptr;
		itemClass = nullptr;
		itemPickupSound = nullptr;
		itemEquipSound = nullptr;
		itemUnEquipSound = nullptr;
		itemDropSound = nullptr;
	}

	void CreateItem(FWeaponInfoStruct weaponInfo);
	//void CreateItem(FArmorInfoStruct weaponInfo);
};
