#pragma once
#include "FCharacterInfo.generated.h"

USTRUCT(BlueprintType)
struct FCharacterInfo : public FTableRowBase {

	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "ClassInfo")
		int32 StartMHP;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "ClassInfo")
		int32 StartMMP;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "ClassInfo")
		int32 StartATK;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "ClassInfo")
		float StartDEF;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "ClassInfo")
		int32 StartLuck;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerAttributes)
		float attackDamage;
	
	//The health of the player
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerAttributes)
	float health;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerAttributes)
	float maxHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerAttributes)
	float mana;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerAttributes)
	float maxMana;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerAttributes)
	float defense;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerAttributes)
	int32 luck;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerAttributes)
	float manaRegenPerSec;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerAttributes)
	float experience;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerAttributes)
	float neededExperience;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerAttributes)
	int playerLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerAttributes)
	int skillPoints;


	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "ClassInfo")
		TArray<FString> StartingAbilities;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "ClassInfo")
		TArray<FString> LearnedAbilities;

};
