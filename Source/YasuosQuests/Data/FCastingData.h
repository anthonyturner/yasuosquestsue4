#pragma once
#include "FCastingData.generated.h"



UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class ECastFailReason : uint8
{
	NoReason 	UMETA(DisplayName = "NoReason"),
	NotEnoughResource1 	UMETA(DisplayName = "NotEnoughResource1"),
	NotEnoughResource2 	UMETA(DisplayName = "NotEnoughResource2"),
	Interrupted 	UMETA(DisplayName = "Interrupted"),
	Silenced	UMETA(DisplayName = "Silenced"),
	NotInControl	UMETA(DisplayName = "NotInControl"),
	GlobalCooldownNotAvailable	UMETA(DisplayName = "GlobalCooldownNotAvailable"),
	AlreadyCasting	UMETA(DisplayName = "AlreadyCasting")

};

USTRUCT(BlueprintType)
struct FCastingData : public FTableRowBase {

	GENERATED_USTRUCT_BODY()

public:

	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Casting)
	FName SpellName;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Casting)
		float CastTime;

 UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Casting)
		bool CanMoveWhileCasting;

 UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Casting)
		float Resource1Use;

 UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Casting)
		float Resource2Use;

 UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Casting)
		float Resource1Gain;

 UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Casting)
		float Resource2Gain;

 UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Casting)
		bool DoesNoTriggerGlobalCD;

 UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Casting)
		float IntermediateEventTime;

 UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Casting)
		float GlobalCDReducedDuration;

 UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Casting)
		bool CanBeUsedNotInControl;

 UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Casting)
		bool CanBeUsedWhileSilenced;


 UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Casting)
		bool CanBeUsedWhileInterrupted;


};
