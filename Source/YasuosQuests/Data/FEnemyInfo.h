#pragma once
#include "FEnemyInfo.generated.h"

class ABaseEnemy;
UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EEnemyType : uint8
{
	Troll 	UMETA(DisplayName = "Troll"),
	Goblin 	UMETA(DisplayName = "Goblin"),
	Human 	UMETA(DisplayName = "Human"),
	Orc 	UMETA(DisplayName = "Orc")
};

USTRUCT(BlueprintType)
struct FEnemyInfoStruct : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		FName enemyName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
	EEnemyType enemyType;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemAttributes)
		TSubclassOf<ABaseEnemy> enemySpawnClass;

	//The health of the enemy 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		float health;

	//The health of the enemy 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		float maxHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		int level;

	//The range for the enemy attack
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		float AttackRange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		float attackChance;

	//The power of the enemy attacks 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		float AttackDamage;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		float AttackSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		float AttackWaitTime;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		FName PrimaryRightHand;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		UParticleSystem* AttackVFX;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		USoundBase* AttackSFX;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		UParticleSystem* HitVFX;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		USoundBase* HitSFX;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		FColor warningColor;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		USoundCue* playerSpottedSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		USoundCue* playerLostSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		UAnimMontage* currentHitAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		UAnimMontage* deathAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		UAnimMontage* standardHitAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		UAnimMontage* backHitAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		UAnimMontage* knockBackHitAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		UAnimMontage* knockDownHitAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EnemyProperties)
		UAnimMontage* lauchHitAnim;


	//default constructor
	FEnemyInfoStruct()
	{


	}
};

