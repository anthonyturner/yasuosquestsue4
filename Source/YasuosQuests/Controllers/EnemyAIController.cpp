// Fill out your copyright notice in the Description page of Project Settings.

#include "YasuosQuests.h"
#include "EnemyAIController.h"
#include "Characters/Enemies/BaseEnemy.h"

AEnemyAIController::AEnemyAIController() {

		// create blackboard and behaviour components in the constructor 
	BlackboardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackBoardComp"));
	BehaviorComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorComp"));
	bWantsPlayerState = true; 

}

void AEnemyAIController::Possess(APawn *InPawn) {

	Super::Possess(InPawn);
	ABaseEnemy* _tempEnemy = Cast<ABaseEnemy>(InPawn);
	// start behavior   


}

void AEnemyAIController::BeginInactiveState()
{
}

void AEnemyAIController::SetEnemy(APawn * InPawn)
{

	if (BlackboardComp) { 

		FName name;
		FString id = FString::FromInt(EnemyPositionKeyID);
		name.AppendString(id);
		BlackboardComp->SetValue<UBlackboardKeyType_Object>(EnemyKeyID, InPawn);
		BlackboardComp->SetValueAsVector(name, InPawn->GetActorLocation());
		SetFocus(InPawn); 
	}
}


AYQCharacter * AEnemyAIController::GetEnemy() const {

	if (BlackboardComp) {

		return Cast<AYQCharacter>(BlackboardComp->GetValue<UBlackboardKeyType_Object>(EnemyKeyID)); 
	}    
	
	return NULL;
}

void AEnemyAIController::UpdateControlRotation(float DeltaTime, bool bUpdatePawn)
{

	// Look toward focus  
	FVector TheCenter = GetFocalPoint();   
	if (!TheCenter.IsZero() && GetPawn())   { 

		FVector Direction = TheCenter - GetPawn()->GetActorLocation();     
		FRotator TheNewRotation = Direction.Rotation();      
		TheNewRotation.Yaw = FRotator::ClampAxis(TheNewRotation.Yaw);      
		SetControlRotation(TheNewRotation);      
		APawn* const _tempPawn = GetPawn();     
		if (_tempPawn &&bUpdatePawn)     {
			_tempPawn->FaceRotation(TheNewRotation, DeltaTime);     
		}    
	} 
}

bool AEnemyAIController::PawnCanBeSeen(APawn * target)
{
	return false;
}

void AEnemyAIController::OnSearchForEnemy(){

		APawn* _tempPawn = GetPawn();   
		if (_tempPawn == NULL) { return; } 
		
		const FVector _tempLocation = _tempPawn->GetActorLocation();
		float BestDistSq = MAX_FLT;   
		AYQCharacter* PlayerPawn = NULL;   
		//foreach all pawns in world   
		for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; ++It) {
			if (PawnCanBeSeen(*It)) {

				AYQCharacter* TestPawn = Cast<AYQCharacter>(*It);
				ABaseEnemy* const _testEnemy = Cast<ABaseEnemy>(TestPawn);
				if (_testEnemy) {
					//it is just another enemy, not player
				}
				else {

					if (TestPawn && TestPawn->GetIsStillAlive()) {
						UE_LOG(LogClass, Log, TEXT(" ===================>>>>>  ENEMY SEEN %s "), *GetNameSafe(*It));
						const float _distanceSq = (TestPawn->GetActorLocation() - _tempLocation).SizeSquared();
						if (_distanceSq < BestDistSq) {
							BestDistSq = _distanceSq;
							PlayerPawn = TestPawn;
						}
					}
				}
			}
		}
}
