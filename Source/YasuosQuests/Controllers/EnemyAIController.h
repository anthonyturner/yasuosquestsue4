// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "Characters/Playable/YQCharacter.h" 
#include "BehaviorTree/BehaviorTree.h" 
#include "BehaviorTree/BehaviorTreeComponent.h" 
#include "BehaviorTree/BlackboardComponent.h" 
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"

#include "EnemyAIController.generated.h"


class ABaseEnemy;
UCLASS()
class YASUOSQUESTS_API AEnemyAIController : public AAIController
{
	GENERATED_BODY()
	
	UPROPERTY(transient)   
		UBlackboardComponent* BlackboardComp;
	
	UPROPERTY(transient)
		UBehaviorTreeComponent* BehaviorComp;

	virtual void Possess(class APawn* InPawn) override;  
	
	virtual void BeginInactiveState() override;
public:

	AEnemyAIController();

	UFUNCTION(BlueprintCallable, Category = Behavior) 
		void SetEnemy(class APawn* InPawn);  
	
	UFUNCTION(BlueprintCallable, Category = Behavior) 
		class AYQCharacter* GetEnemy() const;

	UFUNCTION(BlueprintCallable, Category = Behaviour) 
		void UpdateControlRotation(float DeltaTime, bool bUpdatePawn);  
	
	UFUNCTION(BlueprintCallable, Category = Behaviour) 
		bool PawnCanBeSeen(APawn * target);  
	
	UFUNCTION(BlueprintCallable, Category = Behaviour) 
		void OnSearchForEnemy();


protected: 
	int32 EnemyKeyID;  
	int32 EnemyPositionKeyID;


	
};
