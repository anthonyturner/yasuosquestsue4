// Copyright 2017 @Anthony Turner

#pragma once

#include "Blueprint/UserWidget.h"
#include "EnemyHealthWidget.generated.h"

/**
 * 
 */
UCLASS()
class YASUOSQUESTS_API UEnemyHealthWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, Category = HudDisplay)
		void SetDisplayHealth(float p_health);

	UFUNCTION(BlueprintPure, Category = HudDisplay)
		float GetDisplayHealth();

private:
	float health;
};
