// Copyright 2017 @Anthony Turner

#pragma once

#include "Blueprint/UserWidget.h"
#include "Interfaces/IInventory.h"
#include "InventoryWidget.generated.h"


UCLASS()
class YASUOSQUESTS_API UInventoryWidget : public UUserWidget, public IIInventory
{
	GENERATED_BODY()
	
	

public:


	//UFUNCTION(BlueprintCallable, Category = HudDisplay)
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = InventoryWidget)
		void ClearInventory();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = InventoryWidget)
	void OnWidgetsCreated(const TArray<UInventoryItemWidget*>& widgets);


	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = InventoryWidget)
		void RefreshInventory();

	UFUNCTION(BlueprintCallable, Category = Inventory)
		const TArray<UInventoryItemWidget*> CreateInventory();


	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = InventoryWidget)
	int inventorySize = 25;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = InventoryWidget)
		TSubclassOf<UInventoryItemWidget> InventoryItemWidgetClass;

	virtual void UseItem(FItemInfoStruct itemInfo)  override;
	virtual void UnUseItem(FItemInfoStruct itemInfo)  override;

	virtual void DropItem(FItemInfoStruct itemInfo)  override;

	UFUNCTION(BlueprintCallable, Category = Actions)
		virtual void EnableActionBar(FItemInfoStruct itemInfo)  override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = InventoryWidget)
	void OnEnableActionBar(FItemInfoStruct itemInfo);

	UFUNCTION(BlueprintCallable, Category = InventoryWidget)
	FItemInfoStruct GetItemInfo();

	void SetItemInfo(FItemInfoStruct item);

private:

	FItemInfoStruct selectedItemInfo;
	
};
