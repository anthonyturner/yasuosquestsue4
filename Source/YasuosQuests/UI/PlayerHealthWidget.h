// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "PlayerHealthWidget.generated.h"

/**
 * 
 */
UCLASS()
class YASUOSQUESTS_API UPlayerHealthWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, Category = HudDisplay)
		void SetDisplayHealth(float p_health);

	UFUNCTION(BlueprintPure, Category = HudDisplay)
		float GetDisplayHealth();

	float health;
};
