// Fill out your copyright notice in the Description page of Project Settings.

#include "YasuosQuests.h"
#include "YasuosQuestsGameHUD.h"



void AYasuosQuestsGameHUD::BeginPlay() {

	AddWidgets();
}

void AYasuosQuestsGameHUD::AddWidgets() {

	/*
		if (InventoryUIClass) // Check the selected UI class is not NULL
		{
		if (!inventoryWidget) // If the widget is not created and == NULL
		{
		inventoryWidget = CreateWidget<UInventoryUserWidget>(this, InventoryUIClass); // Create Widget
		if (!inventoryWidget) return;
		inventoryWidget->AddToViewport(); // Add it to the viewport so the Construct() method in the UUserWidget:: is run.
		inventoryWidget->SetVisibility(ESlateVisibility::Visible); // Set it to hidden so its not open on spawn.
		}
		}
	*/

	/*if (HealthWidgetClass) {

		if (!healthWidget) {

			
			healthWidget = CreateWidget<UPlayerHealthWidget>(UGameplayStatics::GetPlayerController(GetWorld(), 0), HealthWidgetClass);
			//if (!healthWidget) { return; }

			healthWidget->AddToViewport();
		}
	}*/

	if (MessageWidgetClass) {

		if (!messageWidget) {


			messageWidget = CreateWidget<UHudMessageWidget>(UGameplayStatics::GetPlayerController(GetWorld(), 0), MessageWidgetClass);
			//if (!healthWidget) { return; }

			messageWidget->AddToViewport();
		}
	}

}

void AYasuosQuestsGameHUD::DrawHUD()
{
	// call superclass DrawHUD() function first
	Super::DrawHUD();
	DrawMessages();
}


void AYasuosQuestsGameHUD::DrawMessages() {

	// iterate from back to front thru the list, so if we remove
	// an item while iterating, there won't be any problems
	for (int c = messages.Num() - 1; c >= 0; c--)
	{
		// draw the background box the right size
		// for the message
		float outputWidth, outputHeight, pad = 10.f;
		GetTextSize(messages[c].message, outputWidth, outputHeight, hudFont, 1.f);

		float messageH = outputHeight + 2.f*pad;
		float x = 0.f, y = c*messageH;

		// black backing
		DrawRect(FLinearColor::Black, x, y, Canvas->SizeX, messageH);
		// draw our message using the hudFont
		DrawText(messages[c].message, messages[c].color, x + 50, y + pad, hudFont);
		
		//Draw avatar
		if (messages[c].tex != nullptr) {

			DrawTexture(messages[c].tex, x, y, messageH, messageH, 0, 0, 1, 1);

		}
		// reduce lifetime by the time that passed since last 
		// frame.
		messages[c].time -= GetWorld()->GetDeltaSeconds();

		// if the message's time is up, remove it
		if (messages[c].time < 0)
		{
			messages.RemoveAt(c);
		}
	}
}

void AYasuosQuestsGameHUD::AddMessage(Message message) {

	messages.Add(message);

}